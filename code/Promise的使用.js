// Promise 基本演示

const promise = new Promise(function (resolve, reject) {
  // 这里用于兑现承诺
  // resolve(100) // 承诺达成

  reject(new Error('promise rejected')) // 承诺失败
})

promise.then(function (value) {
  console.log('resolved', value)
}, function (error) {
  console.log('rejected', error)
})

console.log('end') // 先打印出end，再打印Error
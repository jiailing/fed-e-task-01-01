// 带有默认值的参数要放在最后
function foo(enable = true) {
  console.log(enable)
}
foo(true) //true
foo(false) //false
foo() // true
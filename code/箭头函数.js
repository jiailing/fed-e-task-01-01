// function inc (n) {
//   return n + 1
// }

const inc = n => n + 1
console.log(inc(1)) // 2

const sum = (a, b) => {
  return a + b
}
console.log(sum(1, 2)) // 3

// 箭头函数不会改变this指向
const person = {
  name: 'tom',
  sayHi: function () {
    console.log(`hi, my name is ${this.name}`)
  },
  sayHiAync: function () {
    setTimeout(function () {
      console.log(` sayHiAync: hi, my name is ${this.name}`)
    }, 1000);
  }
}
person.sayHi() // hi, my name is tom
person.sayHiAync() // sayHiAync: hi, my name is undefined

// const person = {
//   name: 'tom',
//   sayHi:  () => {
//     console.log(`hi, my name is ${this.name}`)
//   },
//   sayHiAync: function () {
//     setTimeout(() => {
//       console.log(` sayHiAync: hi, my name is ${this.name}`)
//     }, 1000);
//   }
// }
// person.sayHi() // hi, my name is undefined
// person.sayHiAync() // sayHiAync: hi, my name is tom

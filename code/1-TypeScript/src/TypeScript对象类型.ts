export {} // 确保跟其他实例没有成员冲突

const foo: object = function () {} // [] // {} // object类型并不单指对象，而是指除了原始类型之外的其他类型

const obj: {foo: number, bar: string} = {foo: 123, bar: 'string'} // 对象的赋值必须与定义的属性保持一致，不能多也不能少。更专业的写法是用接口

const arr1: Array<number> = [1, 2, 3]

const arr2: number[] = [1, 2, 3]

function sum (...args: number[]) {
  return args.reduce((prev, current) => prev + current, 0)
}
sum(1, 2, 3)
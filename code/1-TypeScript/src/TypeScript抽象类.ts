export {}

abstract class Animal {
  eat (food: string) : void {
    console.log(`呼噜呼噜的吃：${food}`)
  }

  // 抽象方法不需要方法体，子类必须要实现抽象方法
  abstract run(distance: number): void
}

// 非抽象类“Dog”不会实现继承自“Animal”类的抽象成员“run”
class Dog extends Animal {
  run(distance: number): void {
    console.log(`四脚爬行：${distance}`)
  }
}

const dog = new Dog()
dog.run(20)
dog.eat('fish')
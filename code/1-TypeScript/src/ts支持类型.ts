const a: string = 'foobar'

const b: number = 100 // NaN Infinity

const c: boolean = true // false

// const d: boolean = null // 严格模式下不支持赋值null

const e: void = undefined // 函数没有返回值时的返回值类型

const f: null = null

const g: undefined = undefined

const h: symbol = Symbol()

// const error: string = 100
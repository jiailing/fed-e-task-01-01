export {}

class Person {
  // ES2017定义的语法：
  name: string // = 'init name'
  private age: number
  protected readonly gender: boolean
  constructor(name: string, age: number) {
    this.name = name
    this.age = age
    this.gender = true
  }
  sayHi (msg: string): void {
    console.log(`I am ${this.name}`)
  }
}

const tom = new Person('tom', 18)
console.log(tom.name)
// console.log(tom.age) // 属性“age”为私有属性，只能在类“Person”中访问。
// console.log(tom.gender) // 属性“gender”受保护，只能在类“Person”及其子类中访问。

class Student extends Person {
  private constructor(name: string, age: number) {
    super(name, age)
    console.log(this.gender)
  }
  static create(name: string, age: number) {
    return new Student(name, age)
  }
}

const jack = Student.create('jack', 18)
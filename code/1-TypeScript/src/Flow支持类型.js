/**
 * 原始类型
 * @flow
 */

 const a: string = 'foo'

 const b: number = Infinity // NaN // 100

 const c: boolean = false // true

 const d: null = null

 const e: void = undefined

 const f: symbol = Symbol()

 const arr: Array<number> = [1, 2, 3]

 const arr2: number[] = [1, 2, 3]

 // 元组
 const foo: [string, number] = ['foo', 100]

const obj1: {foo: string, bar: number} = {foo: 'string', bar: 100}

// 问号表示可有可与的属性
const obj2: {foo?: string, bar: number} = {bar: 100}

// 表示当前对象可以添加任意个数的键，不过键值的类型都必须是字符串
const obj3: {[string]: string} = {}
obj3.key1 = 'value1'
// obj3.key2 = 100

function fn (callback: (string, number) => void) {
  callback('string', 100)
}

fn(function (str, n) {

})

const fo: 'foo' = 'foo'

// 联合类型，变量的值只能是其中之一
const type: 'success' | 'warning' | 'danger' = 'success'

// 变量类型只能是其中的一种类型
const g: string | number = 100

type StringOrNumber = string | number
const h: StringOrNumber = 'stri' // 100

// maybe类型 加一个问号，变量除了可以接受number类型以外，还可以接受null或undefined
const gender: ?number = null
// 相当于
// const gender: number | null | void = undefined

// Mixed / Any  mixed是强类型，any是弱类型，为了兼容老代码，是不安全的，尽量不用any
// string | number | boolean |...
function passMixed (value: mixed) {

}
passMixed('string')
passMixed(100)

function passAny (value: any) {

}
passAny('string')
passAny(100)

const element: HTMLElement | null = document.getElementById('root')
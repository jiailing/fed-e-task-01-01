export {}

const nums = [110, 120, 119, 112]

const res = nums.find(i => i>0)
// const res: number | undefined
// const square = res * res

const num1 = res as number // 断言 res 是number
const square = num1 * num1
const num2 = <number>res // JSX下不能使用
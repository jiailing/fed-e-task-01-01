/**
 * 类型推断
 * @flow
 */

 function square ( n: number ) {
   return n * n
 }

 square(100)

 let num: number = 100

 function foo (): void {
   
 }
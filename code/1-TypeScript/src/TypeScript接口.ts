export {}

// 可以用分号分割，分号可以省略
interface Post {
  title: String
  content: String
  subtitle?: string // 可有可无的属性。也就是说该属性为string或者undefined
  readonly summary: string
}

function printPost (post: Post) {
  console.log(post.title)
  console.log(post.content)
}

printPost({
  title: 'hello',
  content: 'javascript',
  summary: 'js'
})

const hello: Post = {
  title: 'hello',
  content: 'javascript',
  summary: 'js'
}

//报错： Cannot assign to 'summary' because it is a read-only property.
// hello.summary = '11'

// 动态成员
interface Cache {
  [prop: string]: string
}

const cache: Cache = {}

cache.foo = 'ff'
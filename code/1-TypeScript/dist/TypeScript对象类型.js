"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var foo = function () { }; // [] // {} // object类型并不单指对象，而是指除了原始类型之外的其他类型
var obj = { foo: 123, bar: 'string' }; // 对象的赋值必须与定义的属性保持一致，不能多也不能少。更专业的写法是用接口
var arr1 = [1, 2, 3];
var arr2 = [1, 2, 3];
function sum() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return args.reduce(function (prev, current) { return prev + current; }, 0);
}
sum(1, 2, 3);
//# sourceMappingURL=TypeScript对象类型.js.map
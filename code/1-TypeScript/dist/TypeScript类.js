"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Person = /** @class */ (function () {
    function Person(name, age) {
        this.name = name;
        this.age = age;
        this.gender = true;
    }
    Person.prototype.sayHi = function (msg) {
        console.log("I am " + this.name);
    };
    return Person;
}());
var tom = new Person('tom', 18);
console.log(tom.name);
// console.log(tom.age) // 属性“age”为私有属性，只能在类“Person”中访问。
// console.log(tom.gender) // 属性“gender”受保护，只能在类“Person”及其子类中访问。
var Student = /** @class */ (function (_super) {
    __extends(Student, _super);
    function Student(name, age) {
        var _this = _super.call(this, name, age) || this;
        console.log(_this.gender);
        return _this;
    }
    Student.create = function (name, age) {
        return new Student(name, age);
    };
    return Student;
}(Person));
var jack = Student.create('jack', 18);
//# sourceMappingURL=TypeScript类.js.map
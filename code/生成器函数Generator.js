// function * foo () {
//   console.log('zce')
//   return 100
// }
// const result = foo()
// console.log(result)// Object [Generator] {}
// console.log(result.next())
// zce
// { value: 100, done: true }
// 可以看出生成器对象实现了Iterator接口

function * fn () {
  console.log(111)
  yield 100
  console.log(222)
  yield 200
  console.log(333)
  yield  300
}

const generator = fn()
// 生成器函数会返回一个生成器对象，调用这个生成器对象的next方法，才会让函数体执行，一旦遇到了yield关键词，函数的执行则会暂停下来，yield后面的值作为next函数的结果返回，如果继续调用函数的next函数，则会再上一次暂停的位置继续执行，知道函数体执行完毕，next返回的对象的done就变成了true
console.log(generator.next())
// 111
// { value: 100, done: false }
console.log(generator.next())
// 222
// { value: 200, done: false }
console.log(generator.next())
// 333
// { value: 300, done: false }
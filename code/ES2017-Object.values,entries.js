const obj = {
  name: 'jal',
  age: 20
}
// 对象的值组成的数组
console.log(Object.values(obj)) // [ 'jal', 20 ]
// 对象的键值数组， 可以for...of 这个对象了
console.log(Object.entries(obj)) // [ [ 'name', 'jal' ], [ 'age', 20 ] ]
for (const [key, value] of Object.entries(obj)) {
  console.log(key, value)
}
// name jal
// age 20

console.log(new Map(Object.entries(obj))) // Map(2) { 'name' => 'jal', 'age' => 20 }

const p1 = {
  firstName: 'Ji',
  lastName: 'Ailing',
  get fullName() {
    return this.firstName + ' '+ this.lastName
  }
}

const p2 = Object.assign({}, p1)
p2.firstName = 'zce'
console.log(p2) // { firstName: 'zce', lastName: 'Ailing', fullName: 'Ji Ailing' }
const descriptors = Object.getOwnPropertyDescriptors(p1)
console.log(descriptors)
/*
{
  firstName: { value: 'Ji', writable: true, enumerable: true, configurable: true },
  lastName: {
    value: 'Ailing',
    writable: true,
    enumerable: true,
    configurable: true
  },
  fullName: {
    get: [Function: get fullName],
    set: undefined,
    enumerable: true,
    configurable: true
  }
}
*/

const p3 = Object.defineProperties({}, descriptors)
p3.firstName = 'zce'
console.log(p3.fullName) // zce Ailing

const books = {
  html: 5,
  css: 16,
  javascript: 128
}
for(const [key, value] of Object.entries(books)) {
  console.log(key, value)
}
// html 5
// css 16
// javascript 128

for(const [key, value] of Object.entries(books)) {
  console.log(`${key.padEnd(16, '-')}|${value.toString().padStart(3, '0')}`)
}
// html------------|005
// css-------------|016
// javascript------|128
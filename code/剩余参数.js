// function foo(){
//   console.log(arguments)
// }
// foo(1, 2, 3) // [Arguments] { '0': 1, '1': 2, '2': 3 }


// ...args只能出现在形参的最后一位，而且只能使用一次
function foo(...args) {
  console.log(args)
}
foo(1, 2, 3) // [ 1, 2, 3 ]
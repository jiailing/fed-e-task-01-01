const obj = {
  foo: '111',
  bar: 'rrr',
  age: 18
}

const proxy = new Proxy(obj, {
  get(target, property) {
    console.log('watch...')
    return Reflect.get(target, property)
  }
})
console.log(proxy.foo)
// watch...
// 111

// console.log("age" in obj)
// console.log(delete obj['bar'])
// console.log(Object.keys(obj))

console.log(Reflect.has(obj, 'name')) // false
console.log(Reflect.deleteProperty(obj, 'bar')) // true
console.log(Reflect.ownKeys(obj)) // [ 'foo', 'age' ]
var element = [{}, {}, {}]
for(var i = 0; i < element.length; i++) {
  element[i].onclick = function () {
    // i是全局变量，已经变成了3
    console.log(i)
  }
}
element[2].onclick() // 3

var element = [{}, {}, {}]
for(var i = 0; i < element.length; i++) {
  element[i].onclick = (function (i) {
  // 闭包实现
    return function () {
      console.log(i)
    }
  })(i)
}
element[2].onclick() // 2

var element = [{}, {}, {}]
for(let i = 0; i < element.length; i++) {
  // let定义的变量是块级作用域
  element[i].onclick = function () {
    console.log(i)
  }
}
element[2].onclick() // 2
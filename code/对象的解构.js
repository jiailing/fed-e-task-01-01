const obj = {name: 'yibo', age: 22}
// const {name} = obj
// console.log(name) // yibo

// const name = 'jal'
// const {name} = obj
// console.log(name) // SyntaxError: Identifier 'name' has already been declared

const name = 'jal'
const {name: objName, sex = 'boy'} = obj
console.log(objName, sex) // yibo boy


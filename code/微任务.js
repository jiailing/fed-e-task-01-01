console.log('global start') // 第一个打印

setTimeout(() => {
  console.log('last') // 最后调用
}, 0);
Promise.resolve()
.then(()=>{
  console.log('promise')// 第3个打印
})
.then(()=>{
  console.log('promise 2')// 第4个打印
})
.then(()=>{
  console.log('promise 3')// 第5个打印
})
console.log('global end')// 第2个打印
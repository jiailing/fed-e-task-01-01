// 迭代器 iterator 
const set = new Set(['foo', 'bar', 'baz'])

const iterator = set[Symbol.iterator]()

// 这就是for... of 循环实现的工作原理
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())
// { value: 'foo', done: false }
// { value: 'bar', done: false }
// { value: 'baz', done: false }
// { value: undefined, done: true }

// obj 实现可迭代接口 Iterable
const obj = {
  // iterator 方法
  [Symbol.iterator]: function () {
    // 迭代器接口 iterator 
    return {
      // 必须要有next方法
      next: function () {
        // 迭代结果接口 IterationResult
        return {
          value: 1,
          done: true
        }
      }
    }
  }
}

const obj = {
  store: ['foo', 'bar', 'baz'],

  [Symbol.iterator]: function () {
    let index = 0
    const self = this

    return {
      next: function () {
        const result = {
          value: self.store[index],
          done: index >= self.store.length
        }
        index++
        return result
      }
    }
  }
}

for( const item of obj) {
  console.log(item)
}
// foo
// bar
// baz
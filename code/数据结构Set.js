// Set 数据结构
const s = new Set()
s.add(1).add(2).add(3).add(4).add(2)
console.log(s) // Set(4) { 1, 2, 3, 4 }

s.forEach(i => console.log(i)) // 1 2 3 4

for(let i of s) {
  console.log(i)
}
// 1 2 3 4

console.log(s.size) // 4

console.log(s.delete(3)) // true
console.log(s) // Set(3) { 1, 2, 4 }

const arr = [1, 2, 1, 3, 4 ,1]
const result = new Set(arr)
console.log(result) // Set(4) { 1, 2, 3, 4 }
const arr2 = Array.from(result)
console.log(arr2) // [ 1, 2, 3, 4 ]
const arr3 = [...result]
console.log(arr3) // [ 1, 2, 3, 4 ]
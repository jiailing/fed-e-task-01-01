// function Person(name) {
//   this.name = name
// }
// Person.prototype.say = function() {
//   console.log(`hi, my name is ${this.name}`)
// }
// const p = new Person('jal')
// p.say() // hi, my name is jal

class Person {
  constructor(name) {
    this.name = name
  }
  say () {
  console.log(`hi, my name is ${this.name}`)
  }
  static create(name) {
    // this 指向当前类型，而不是实例
    console.log(this) // [Function: Person]
    return new Person(name)
  }
}
const p = new Person('jal')
p.say() // hi, my name is jal

const tom = Person.create('tom')
tom.say() // hi, my name is tom

class Student extends Person {
  constructor(name, number){
    super(name)
    this.number = number
  }

  hello () {
    super.say()
    console.log(`my school number is ${this.number}`)
  }
}

const s = new Student('jack', 100)
s.hello()
// hi, my name is jack
// my school number is 100